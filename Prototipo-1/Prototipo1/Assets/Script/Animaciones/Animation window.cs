using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animationwindow : MonoBehaviour
{
    public Animator Prop;
  private AudioSource audioSource;
    public AudioClip Abrir;
    public AudioClip Cerrar;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();

        if (audioSource == null )
        {
            audioSource=gameObject.AddComponent<AudioSource>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Prop.Play("open");
        Debug.Log("Animacion de abrir");
        audioSource.clip = Abrir;
        audioSource.Play();


    }

    private void OnTriggerExit(Collider other)
    {
        Prop.Play("close");
        Debug.Log("Animacion de cerrar");
        audioSource.clip = Cerrar;
        Invoke("playAudio", 0.5f);
    }

    void playAudio ()
    { audioSource.Play(); }
}
